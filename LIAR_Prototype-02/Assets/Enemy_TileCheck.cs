using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_TileCheck : MonoBehaviour
{
    public TileStat currentTile;
    public List<TileStat> allTiles;
    public List<TileStat> canMoveTiles;
    public float moveRange;
    private void OnTriggerEnter(Collider other)
    {
        TileStat t = other.GetComponent<TileStat>();
        if (t != null)
        {
            currentTile = t;
        }
    }

    public void FindNearbyTiles()
    {
        canMoveTiles.Clear();
        ClearAllTiles();
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, moveRange);

        foreach (var hitCollider in hitColliders)
        {
            if (hitCollider.GetComponent<TileStat>() != null)
            {
                TileStat t = hitCollider.GetComponent<TileStat>();
                canMoveTiles.Add(t);
                t.isInMoveTrigger = true;
            }

        }
        canMoveTiles.Remove(currentTile);

    }

    public void ClearAllTiles()
    {
        foreach (TileStat t in allTiles)
        {
            t.isInMoveTrigger = false;
            t.moveDistFromPlayer = 0;
        }
    }
}
