using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerActionManager : MonoBehaviour
{

    public int leadEntity;
    public bool playerIsLead;

    public float p1ActionPoints;
    public float p2ActionPoints;

    public Slider p1AP_slider;
    public TextMeshProUGUI p1AP_text;
    public Slider p2AP_slider;
    public TextMeshProUGUI p2AP_text;

    public Rama_MovementControl ramaMoveControl;
    public Rama_ShootWeapon ramaShootControl;
    public TurnManager turnStats;
    // Start is called before the first frame update
    void Start()
    {
        // 0 --> Alex turn
        // 1 --> Rama turn
        leadEntity = 0;
        p1ActionPoints = 8;
        p2ActionPoints = 8;
    }

    private void Update()
    {
        p1AP_slider.value = p1ActionPoints / 8;
        p1AP_text.text = "AP: " + p1ActionPoints + " / " + 8;

        p2AP_slider.value = p2ActionPoints / 8;
        p2AP_text.text = "AP: " + p2ActionPoints + " / " + 8;
    }

    public void TurnInit()
    {
        p1ActionPoints = 8;
        p2ActionPoints = 8;
    }

    public IEnumerator DoRamaActions()
    {
        while (p2ActionPoints > 0)
        {
            int actions = 3;
            int rAction = Random.Range(0, actions);

            if (rAction == 0 && p2ActionPoints >= 2) //Choose to move;
            {
                ramaMoveControl.eTileCheck.FindNearbyTiles();
                ramaMoveControl.MoveToRandomTile();
                print("Rama Chose to move");
                p2ActionPoints -= 2;
                yield return new WaitForSeconds(1f);
            }
            else if (rAction == 1 && p2ActionPoints >= 2 )// Choose to shoot
            {
                print("Rama Chose to shoot");
                ramaShootControl.FindTargets();
                ramaShootControl.TakeAimAtTarget();
                yield return new WaitForSeconds(1f);
                p2ActionPoints -= 2;
                ramaShootControl.FireWeapon();
                yield return new WaitForSeconds(0.5f);
            }
            else if (rAction == 3 && p2ActionPoints >= 3)// Choose to shoot
            {
                print("Rama Chose to ability");
                p2ActionPoints -= 3;
                yield return new WaitForSeconds(1f);
            }
            else
            {
                print("Rama No Valid Choice");
                //break;
            }
        }
        turnStats.RamaTurnComplete();


        //    yield return new WaitForSeconds(1f);
        //    eShoot.FireWeapon();                               //Enemy Fires at aimed target
        //    eShoot.inAimMode = false;
        //    eShoot.UpdateAimRay();
        //    enemyTurns++;                           //enemies[enemyTurns].MoveToLocation();                         
        //}
        //yield return new WaitForSeconds(0.5f)

    }
}
