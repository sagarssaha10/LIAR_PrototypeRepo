using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using MoreMountains.Feedbacks;

public class EnemyHeathManager : MonoBehaviour
{
    public float HP;
    public float maxHP;
    public Image hpBar;
    public Enemy_ShootWeapon eShootWeapon;
    //public MMFeedbacks hitfeedback;
    //public MMFeedbacks deathfeedback;
    private EnemyActionsManager enemiesManager;

    private void Start()
    {
        enemiesManager = FindObjectOfType<EnemyActionsManager>();
        HP = maxHP;
    }
    private void Update()
    {
        hpBar.fillAmount = HP / maxHP;
    }
    public void TakeDamage(float dmg)
    {
        HP -= dmg;
        eShootWeapon.TakeAimAtTarget();
        if (HP <= 0)
        {
            enemiesManager.RemoveEnemyFromList(this.gameObject.GetComponent<EnemyMovement>());
            gameObject.SetActive(false);
            //deathfeedback.PlayFeedbacks();
        }
        else
        {
            //hitfeedback.PlayFeedbacks();
        }
     
    }
}
