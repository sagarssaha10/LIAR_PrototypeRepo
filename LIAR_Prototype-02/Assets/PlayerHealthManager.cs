using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
//using MoreMountains.Feedbacks;

public class PlayerHealthManager : MonoBehaviour
{
    public float health;
    public float maxHealth;

    public Slider hpBar;
    public TextMeshProUGUI hpText;
    //public MMFeedbacks damageFeedback;
    void Start()
    {
        health = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        hpBar.value = health / maxHealth;
        hpText.text = "HP: " + health + " / " + maxHealth;
    }

    public void TakeDamage(float dmg)
    {
        health -= dmg;
        //damageFeedback.PlayFeedbacks();

        if (health <= 0)
        {
            health = 0;
            KnockedDown();
        }
        
    }

    public void KnockedDown()
    {
        transform.localEulerAngles = new Vector3(90, 0, 0);
    }
}
