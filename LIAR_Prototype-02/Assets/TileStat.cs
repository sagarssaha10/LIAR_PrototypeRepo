using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TileStat : MonoBehaviour
{
    public MeshRenderer tileMesh;

    public Color onIdle;
    public Color onHover;
    public Color onSelect;

    public bool isHighlighted;
    public bool isSelected;
    public bool isInMoveTrigger;
    public float moveDistFromPlayer;


    private NavMeshPath pathToPlayer;
    private Transform target;
    public bool distanceFound;
    // Start is called before the first frame update
    void Start()
    {
        tileMesh = GetComponent<MeshRenderer>();
        //target = FindObjectOfType<PlayerControl>().transform;
        //pathToPlayer = new NavMeshPath();
        //distanceFound = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        //isHighlighted = moveDistFromPlayer < 18;
        if (isInMoveTrigger)
        {
           
                isHighlighted = true;
            
        }
        else
        {
            isHighlighted = false;
        }


        if (isHighlighted)
        {
            tileMesh.material.color = onHover;
            //FindTravelDistToPlayer();
            //if (Input.GetMouseButtonDown(0))
            //{
            //    isSelected = true;
            //    isHighlighted = false;
            //}
        }
        else if (isSelected)
        {
            tileMesh.material.color = onSelect;
        }
        else
        {
            tileMesh.material.color = onIdle;
        }

    }

    //public float FindTravelDistToPlayer()
    //{
     
    //    NavMesh.CalculatePath(transform.position, target.position, NavMesh.AllAreas, pathToPlayer);
    //    float dist = 0;
    //    for (int i = 0; i < pathToPlayer.corners.Length-1; i++)
    //    {
    //        Debug.DrawLine(pathToPlayer.corners[i], pathToPlayer.corners[i+1], Color.blue);
    //        dist += Vector3.Distance(pathToPlayer.corners[i], pathToPlayer.corners[i + 1]);
    //    }
    //    return dist;
    //    //distanceFound = true;
    //}
    //private void OnMouseEnter()
    //{
    //    if(!isSelected)
    //        isHighlighted = true;

    //}

    //private void OnMouseExit()
    //{
    //    isHighlighted = false;
    //}
}
