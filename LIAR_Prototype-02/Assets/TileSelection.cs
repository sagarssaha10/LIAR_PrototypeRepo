using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSelection : MonoBehaviour
{
    public GameObject highlightedTile;
    private Camera mainCam;

    private TileStat tileStat;
    // Start is called before the first frame update
    void Start()
    {
        mainCam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        //Ray ray = mainCam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            
            highlightedTile = hit.transform.gameObject;
            //print("I'm looking at " + highlightedTile.transform.name + " at distance: " + highlightedTile.GetComponent<TileStat>().moveDistFromPlayer);
            
            //tileStat = highlightedTile.GetComponent<TileStat>();
            //tileStat.isHighlighted = true;
        }
        
    }
}
