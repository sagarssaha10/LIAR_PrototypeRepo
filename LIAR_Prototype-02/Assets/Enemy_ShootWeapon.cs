using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_ShootWeapon : MonoBehaviour
{
    public PlayerHealthManager[] targets;
    public Transform aimRay;
    public Transform shootPoint;
    public GameObject bullet;
    public bool inAimMode;
    // Start is called before the first frame update
    void Start()
    {
        FindTargets();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            TakeAimAtTarget();
        }

        if (inAimMode)
        {
            
            if(Input.GetKeyDown(KeyCode.R))
            {
                FireWeapon();
                inAimMode = false;
                UpdateAimRay();
            }
        }
    }
    public void TakeAimAtTarget()
    {
        inAimMode = true;
        aimRay.LookAt(targets[Random.Range(0,2)].transform);
        UpdateAimRay();
    }
    public void FireWeapon()
    {
        GameObject b = Instantiate(bullet, shootPoint.position, shootPoint.rotation);
        b.GetComponent<BulletDamage>().isEnemyAttack = true;
        b.GetComponent<Rigidbody>().AddForce(shootPoint.forward * 100, ForceMode.Impulse);
        //pActions.p1ActionPoints -= 2;
        inAimMode = false;
        UpdateAimRay();
    }

    public void UpdateAimRay()
    {
        //pointer.gameObject.SetActive(inAimMode);
        aimRay.gameObject.SetActive(inAimMode);
    }

    public void FindTargets()
    {
        targets = FindObjectsOfType<PlayerHealthManager>();
    }
}
