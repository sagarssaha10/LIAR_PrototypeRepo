using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyActionsManager : MonoBehaviour
{
    public List<EnemyMovement> enemies;
    public int enemyTurns;
    public TurnManager turnStats;
    //public Enemy_ShootWeapon eShootWeapon;
    private void Start()
    {

        //StartCoroutine(SetupEnemyActions());
        enemyTurns = 0;
    }

    public IEnumerator SetupEnemyActions()
    {
        enemyTurns = 0;
        while (enemyTurns < enemies.Count)
        {
            //print("E" + enemyTurns + " finds tiles");
            Enemy_ShootWeapon eShoot = enemies[enemyTurns].GetComponentInChildren<Enemy_ShootWeapon>();
            eShoot.FindTargets();
            enemies[enemyTurns].turnOrderIndex.text = (enemyTurns+1).ToString();



            if (eShoot.targets.Length == 0)
            {
                print("No Players in Sight, Choosing to Move");
            }
            else
            {

                print("E" + enemyTurns + "chose to move");
                enemies[enemyTurns].eTileCheck.FindNearbyTiles();             //Enemy finds tiles to move

                yield return new WaitForSeconds(0.5f);
                enemies[enemyTurns].MoveToRandomTile();                      //Enemy Moves to the chosen location          

                yield return new WaitForSeconds(1f);
                print("Enemy shoots target");
                eShoot.TakeAimAtTarget();                               //Enemy Takes aim at target
                //enemies[enemyTurns].MoveToLocation();
                //yield return new WaitForSeconds(1f);
                //eShoot.FireWeapon();                               //Enemy Fires at aimed target
                //eShoot.inAimMode = false;
                //eShoot.UpdateAimRay();
            }
            enemyTurns++;
        }

        yield return new WaitForSeconds(0.5f);
        turnStats.EnemyInitiateComplete();
        //turnStats.EnemyTurnComplete();
        //shootUnitManager.camShiftSystem.ResetToFollowCam();

    }

    public IEnumerator CompleteEnemyActions()
    {
        turnStats.turnIndex = 3;
        enemyTurns = 0;
        while (enemyTurns < enemies.Count)
        {
            Enemy_ShootWeapon eShoot = enemies[enemyTurns].GetComponentInChildren<Enemy_ShootWeapon>();
            yield return new WaitForSeconds(1f);
            eShoot.FireWeapon();                               //Enemy Fires at aimed target
            eShoot.inAimMode = false;
            eShoot.UpdateAimRay();
            enemyTurns++;                           //enemies[enemyTurns].MoveToLocation();                         
        }
        yield return new WaitForSeconds(0.5f);
        
        turnStats.EnemyInitiate();
        //turnStats.EnemyTurnComplete();
        //shootUnitManager.camShiftSystem.ResetToFollowCam();

    }

    public void RemoveEnemyFromList(EnemyMovement enemy)
    {
        enemies.Remove(enemy);
    }

    public void AddEnemyToList(EnemyMovement enemy)
    {
        enemies.Add(enemy);
    }
}
