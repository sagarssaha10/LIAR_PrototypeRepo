using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDamage : MonoBehaviour
{
    public bool isEnemyAttack;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Obstacle"))
        {
            Destroy(this.gameObject);
        }

        if (isEnemyAttack)
        {
            PlayerHealthManager pHP = other.GetComponent<PlayerHealthManager>();
            if (pHP != null)
            {
                pHP.TakeDamage(Random.Range(15, 20));
            }
        }
        else
        {
            EnemyHeathManager eHP = other.GetComponent<EnemyHeathManager>();
            if (eHP != null)
            {
                eHP.TakeDamage(Random.Range(22, 36));
            }
        }

        
    }
}
