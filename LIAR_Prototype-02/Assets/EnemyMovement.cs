using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;

public class EnemyMovement : MonoBehaviour
{
    NavMeshAgent myNav;
    public float moveRange;
    public Enemy_TileCheck eTileCheck;
    public TextMeshProUGUI turnOrderIndex;
    
    // Start is called before the first frame update
    void Start()
    {
        myNav = GetComponent<NavMeshAgent>();
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
           eTileCheck.FindNearbyTiles();
            MoveToRandomTile();
        }
        turnOrderIndex.gameObject.SetActive(Input.GetKey(KeyCode.LeftAlt));
    }
    public void MoveToRandomTile()
    {
        int r = Random.Range(0, eTileCheck.canMoveTiles.Count);
        TileStat targetTile = eTileCheck.canMoveTiles[r];
        myNav.SetDestination(targetTile.transform.position);
    }
    

   
}
