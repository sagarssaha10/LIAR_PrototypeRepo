using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTileCheck : MonoBehaviour
{
    PlayerActionManager pActions;
    // Start is called before the first frame update
    public TileStat currentTile;
    TileStat newTile;

    private void Start()
    {
        pActions = FindObjectOfType<PlayerActionManager>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<TileStat>() != null)
        {
            newTile = other.gameObject.GetComponent<TileStat>();

            
        }
       
    }

    private void OnTriggerExit(Collider other)
    {
        if (currentTile != newTile)
        {
            pActions.p1ActionPoints--;
            currentTile = newTile;
        }
        //print(ap);
    }
}
