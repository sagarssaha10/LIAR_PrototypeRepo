using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootWeapon : MonoBehaviour
{

    RaycastHit hit;
    public Transform pointer;
    public Transform aimRay;
    public Transform shootPoint;

    public bool inAimMode;

    public GameObject bullet;

    PlayerActionManager pActions;

    private void Start()
    {
        pActions = FindObjectOfType<PlayerActionManager>();
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            ActivateAim();
        }
        
        
        if(inAimMode)
        {
            
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                TileStat tileHit = hit.transform.GetComponent<TileStat>();
                //if (tileHit != null)
                //{
                    pointer.position = hit.point;
                //}

                // Do something with the object that was hit by the raycast.
            }
            aimRay.LookAt(pointer);
            aimRay.localEulerAngles = new Vector3(0, aimRay.localEulerAngles.y, 0);

            if(Input.GetMouseButtonDown(0))
            {
                if(pActions.p1ActionPoints >= 2)
                {
                    FireWeapon();
                    
                }
                inAimMode = false;
                UpdateAimRay();

            }
        }
    }
    public void FireWeapon()
    {
        GameObject b = Instantiate(bullet, shootPoint.position, shootPoint.rotation);
        b.GetComponent<Rigidbody>().AddForce(shootPoint.forward * 100, ForceMode.Impulse);
        pActions.p1ActionPoints -= 2;
        
    }

    public void ActivateAim()
    {
        inAimMode = !inAimMode;
        UpdateAimRay();
    }
    void UpdateAimRay()
    {
        pointer.gameObject.SetActive(inAimMode);
        aimRay.gameObject.SetActive(inAimMode);
    }
}
