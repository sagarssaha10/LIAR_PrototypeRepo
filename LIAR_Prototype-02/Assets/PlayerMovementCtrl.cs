using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovementCtrl : MonoBehaviour
{
    public float speed;
    public float distCovered;
    public bool canMove;
    public bool ActivateSmoothMovement;
    public bool isActive;

    RaycastHit hit;
    NavMeshAgent myNav;
    TileSelection tileStats;
    public PlayerTileCheck tileCheck;
    public PlayerActionManager playerActions;
    private void Start()
    {
        myNav = GetComponent<NavMeshAgent>();
    }
    private void Update()
    {
        canMove = playerActions.p1ActionPoints > 0;
        if(!ActivateSmoothMovement)
        {
            if (Input.GetMouseButton(0) && canMove)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit))
                {
                    TileStat tileHit = hit.transform.GetComponent<TileStat>();
                    if (tileHit != null)
                    {
                        //tileHit.isSelected = true;
                        myNav.SetDestination(tileHit.transform.position);
                    }

                    // Do something with the object that was hit by the raycast.
                }
            }
        }
        else
        {
            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");
            Vector3 inputDir = new Vector3(x, 0, y);

            if (inputDir.magnitude > 0.38f && canMove && isActive)
            {
                myNav.Move(inputDir.normalized * speed * Time.deltaTime);
                distCovered += Time.deltaTime;
                //if (currentTile != null)
                //    navAgent.SetDestination(currentTile.transform.position + inputDir * 3);
                myNav.velocity *= Time.deltaTime;
                if (tileCheck.currentTile != null)
                {
                    transform.LookAt(tileCheck.currentTile.transform.position + inputDir * 4);
                    transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);
                }
                else
                {
                    transform.LookAt(transform.position + inputDir * 4);
                    transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);
                }

            }
            else if (tileCheck.currentTile != null)
            {
                myNav.SetDestination(tileCheck.currentTile.transform.position);
            }
        }
    }
}
