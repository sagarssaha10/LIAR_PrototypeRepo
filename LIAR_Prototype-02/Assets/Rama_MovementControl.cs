using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Rama_MovementControl : MonoBehaviour
{
    NavMeshAgent myNav;
    public Enemy_TileCheck eTileCheck;
    public bool isActive;

    // Start is called before the first frame update
    void Start()
    {
        myNav = GetComponent<NavMeshAgent>();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            eTileCheck.FindNearbyTiles();
            MoveToRandomTile();
        }
    }
    public void MoveToRandomTile()
    {
        if(isActive)
        {
            int r = Random.Range(0, eTileCheck.canMoveTiles.Count);
            TileStat targetTile = eTileCheck.canMoveTiles[r];
            myNav.SetDestination(targetTile.transform.position);
            
        }
        
    }
}
