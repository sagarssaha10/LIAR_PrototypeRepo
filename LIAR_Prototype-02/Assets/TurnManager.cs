using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class TurnManager : MonoBehaviour
{
    public int turnIndex;
    public bool AlexIsLead;

    public TextMeshProUGUI turnState;
    public TextMeshProUGUI playerOrder;
    public Image TurnPanel;
    public List<Color> colors;

    public EnemyActionsManager EnemyActions;
    public PlayerActionManager PlayerActions;
    public PlayerMovementCtrl alexMove;
    public Rama_MovementControl ramaMove;
    // Start is called before the first frame update
    void Start()
    {
        // 0 ---> Game Init
        // 1 ---> Enemy decide intent
        // 2 ---> Player Action Phase
        // 3 ---> Enemy Execute
        turnIndex = 0;
        EnemyInitiate();
        UpdateLead();
    }

    private void Update()
    {


        if (turnIndex == 1)
        {
            turnState.text = "Enemy Planning";
            //TurnPanel.color = colors[2];
        }
        else if (turnIndex == 2)
        {
            turnState.text = " Player Phase";
            if (AlexIsLead)
            {
                //TurnPanel.color = colors[0];
                playerOrder.text = "Alex --> Rama";
            }
            else
            {
                //TurnPanel.color = colors[1];
                playerOrder.text = "Rama --> Alex";
            }
        }
        else if (turnIndex == 3)
        {
            turnState.text = "Enemy Execute";
            //TurnPanel.color = colors[2];
        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
            AlexTurnComplete();
        }
        if (Input.GetKeyDown(KeyCode.RightShift))
        {
            SwitchLead();
        }
    }

    public void EnemyInitiate()
    {
        turnIndex = 1;
        TurnPanel.color = colors[2];
        EnemyActions.StopAllCoroutines();
        EnemyActions.StartCoroutine(EnemyActions.SetupEnemyActions());
    }
    public void EnemyInitiateComplete()
    {
        turnIndex = 2;
        PlayerActions.TurnInit();
        //int leadCheck = 1;
        //int leadCheck = Random.Range(0, 2);                   //Randomize Lead role per turn
        //AlexIsLead = leadCheck == 0;
        if (AlexIsLead)
            AlexTurnStart();

        else
            RamaTurnStart();

    }

    public void AlexTurnStart()
    {
        alexMove.isActive = true;
        ramaMove.isActive = false;
        TurnPanel.color = colors[0];

    }
    public void RamaTurnStart()
    {
        alexMove.isActive = false;
        ramaMove.isActive = true;
        TurnPanel.color = colors[1];
        PlayerActions.StopAllCoroutines();
        PlayerActions.StartCoroutine(PlayerActions.DoRamaActions());


    }
    public void RamaTurnComplete()
    {
        if (AlexIsLead)
            EnemyExecuteActions();
        
        else
            AlexTurnStart();
        
    }

    public void AlexTurnComplete()
    {
        if (AlexIsLead)
        {
            RamaTurnStart();
        }
        else
        {
            EnemyExecuteActions();
        }

    }

    public void EnemyExecuteActions()
    {
        turnIndex = 3;
        TurnPanel.color = colors[2];
        EnemyActions.StopAllCoroutines();
        EnemyActions.StartCoroutine(EnemyActions.CompleteEnemyActions());
    }


    public void SwitchLead()
    {
        AlexIsLead = !AlexIsLead;
        UpdateLead();


    }
    void UpdateLead()
    {
        if (AlexIsLead)
        {

            playerOrder.text = "Alex --> Rama";
        }
        else
        {
            playerOrder.text = "Rama --> Alex";
        }
    }
}
